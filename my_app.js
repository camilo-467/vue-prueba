new Vue({
    el:'#app',
    data: {
        house:'',
        wizards:[],
        search:'',
        order:'',
    },
    methods: {
        CargarDatosDesdeServidor: function (home) {
            self = this
            if (home!='Humans') {
                axios.get('http://hp-api.herokuapp.com/api/characters/house/'+home).then(
                    function (response) {
                        self.wizards = response.data;
                    }
                )
            }else{
                axios.get('http://hp-api.herokuapp.com/api/characters').then(
                    function (response) {
                        self.wizards = response.data;
                        let wiz = [];
                        for (var i = 0; self.wizards.length > i ; i++) {
                            if (self.wizards[i].house == '') {
                                wiz.push(self.wizards[i]);
                            }
                        }
                        self.wizards = wiz;
                    }
                )
            }
            
        },
        BuscarMago: function(sch) {
            this.buscar
            let self = this
            axios.get('http://hp-api.herokuapp.com/api/characters').then(
                function (response) {
                    self.wizards = response.data.filter(
                        (wiz) => {
                            return wiz.name.toLowerCase().indexOf(sch.toLowerCase())>=0
                        }
                    )
                }
            )
        },
        OrderWizards: function(order) {
            if ( order == 'Nombre' ) {
                this.wizards = this.wizards.sort((a, b) => a.name > b.name);
            }
            if ( order == 'Vida' ) {
                this.wizards = this.wizards.sort((a, b) => a.alive < b.alive);
            }
            if ( order == 'Apellido') {
                for (var i = 0; this.wizards.length > i ; i++) {
                    let names = this.wizards[i].name.split(" ");
                    let firtsname = names[0];
                    let lastname = names[1];
                    this.wizards[i].name = lastname + ' ' + firtsname;
                }
                this.wizards = this.wizards.sort((a, b) => a.name > b.name);
            }
            this.order = '';
        }
    },
    watch: {
        house: function() {
            this.CargarDatosDesdeServidor(this.house);
        },
        search: function() {
            this.house = "Resultados...";
            this.BuscarMago(this.search);
        },
        order: function() {
            if( this.order != '' ) {
                this.OrderWizards(this.order);
            }  
        }
    }
});